import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import { sync } from 'vuex-router-sync'

import plugins from './config/plugins'
import configRouter from './config/router'
import messages from './config/languages'
import guards from './config/guards'
import vuexStore from './vuex/store'

import 'vue-material/dist/vue-material.css'

import App from './App'
import './filter'
import './assets/style.scss'

Vue.config.productionTip = false
const store = new Vuex.Store(vuexStore)
const router = configRouter(VueRouter)

plugins(Vue, VueI18n, Vuex, VueRouter)
guards(router)
sync(store, router)

/* eslint-disable no-new */
const i18n = new VueI18n({
  locale: 'th',
  messages
})

new Vue({
  router,
  i18n,
  el: '#app',
  render: h => h(App)
}).$mount('#app')
