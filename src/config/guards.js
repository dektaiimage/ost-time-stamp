import ls from 'local-storage'
export default (router) => {
  router.beforeEach((to, from, next) => {
    const user = ls.get('user')
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      if (user) {
        next()
      } else {
        next({
          name: 'Login',
          query: {
            redirect: to.fullPath
          }
        })
      }
    }
    next()
  })
}
