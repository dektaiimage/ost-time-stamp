// import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import axios from 'axios'

export default (Vue, ...params) => {
  params.filter((el) => {
    Vue.use(el)
  })
  //   params.filter(el => typeof el === 'object')
  //     .map(le => Vue.use(le))
  Vue.use(VueMaterial)
  Vue.use(VeeValidate)
  Vue.use(VueRouter)
  Vue.use(axios)
}
