import th from './th.json'
import en from './en.json'
export default {
  en: {
    message: en
  },
  th: {
    message: th
  }
}
