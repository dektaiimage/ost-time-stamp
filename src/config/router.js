import Dashboard from '../containers/Home'
import Login from '../containers/login/Login'
import Timestamp from '../containers/timestamp/Timestamp'
import History from '../containers/timestamp/History'
import HistoryView from '../containers/timestamp/HistoryView'

const routes = [
  { path: '/login', name: 'Login', component: Login },
  { path: '/',
    component: Dashboard,
    redirect: '/login',
    children: [
      {path: 'timestamp', name: 'Timestamp', component: Timestamp, meta: { requiresAuth: true }},
      {path: 'history', name: 'History', component: History, meta: { requiresAuth: true }},
      {path: '/history/:id', component: HistoryView, meta: { requiresAuth: true }}
    ]
  },
  { path: '*', redirect: '/' }
]
export default VueRouter => new VueRouter({
  routes,
  mode: 'history',
  saveScrollPosition: true,
  transitionOnLoad: true,
  linkActiveClass: 'is-active',
  history: true
})
