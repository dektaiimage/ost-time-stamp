import Vue from 'vue'
import moment from 'moment'
import ls from 'local-storage'

moment.locale(ls.get('locale') || 'th')
Vue.filter('upper', (text) => {
  if (typeof text !== 'string') {
    return ''
  }
  return text.toUpperCase()
})
Vue.filter('formatDate', (value) => {
  if (value) {
    return (moment(value).format('D') + ' ' + moment(value).format('MMMM') + ' ' + moment(value).format('BBBB'))
  }
})

Vue.filter('formatTime', (value) => {
  if (value) {
    return (moment(value).format('hh:mm:ss')) // hh:mm:ss a : ก่อนเที่ยง
  }
})

Vue.filter('formatDateTime', (value) => {
  if (value) {
    return (moment(value).format('LLLL'))
  }
})